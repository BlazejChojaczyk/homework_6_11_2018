class Furniture:
    def __init__(self):
        print("Creating Furniture")

    def stores_things(self):
        print("Jakie rzeczy przechowuje?")

    def thing_dead(self):
        print("Jest to rzecz martwa")

furniture = Furniture()
furniture.stores_things()
furniture.thing_dead()

class ShoeCabinet(Furniture):
    def __init__(self):
        print("Creating Bedside Cabinet")

    def stores_things(self):
        print("Przechowuje buty")

class KitchenCabinet(Furniture):
    def __init__(self):
        print("Creating Kitchen Cabinet")

    def stores_things(self):
        print("Przechowuje naczynia")

shoe_cabinet = ShoeCabinet()
kitchen_cabinet = KitchenCabinet()

shoe_cabinet.stores_things()
kitchen_cabinet.stores_things()

shoe_cabinet.thing_dead()
kitchen_cabinet.thing_dead()
class Human:
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname
        print("Human with name {} {} born. Let's celebrate!".format(self.firstname, self.lastname))

    def __del__(self):
        print("Human with name {} {} died. RIP.".format(self.firstname, self.lastname))
        self.firstname = ""
        self.lastname = ""

damian = Human("Damian", "Giebas")
del damian

class Programmer(Human):
    def __init__(self, firstname, lastname, programming_language):
        super(Programmer, self).__init__(firstname, lastname)
        print("There is nothing to celebrate. It's just programmer.")
        self.programming_language = programming_language

    def __del__(self):
        print("How sad. This guy just create Facebook.")
        self.programming_language = ""


mark = Programmer("Mark", "Zuckerberg", "PHP")
del mark

class Musician(Human):
    def __init__(self, firstname, lastname, instrument):
        super(Musician, self).__init__(firstname, lastname)
        print("This person is a musician.")
        self.instrument = instrument

    def __del__(self):
        print("This guy plays trumpet.")
        self.instrument = ""


louis = Musician("Louis", "Armstrong", "Trumpet")